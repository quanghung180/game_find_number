var GameViewer = function (row,col,array) {
  let game = null;
  let card = null;
  let modal = null;
  let open = [];
  let stack = [];
  (function init() {
    game = document.getElementById('game');
    setRowsItem(row,col,array);
  })();

  function setRowsItem(row, col, array) {
    let counter = 0;
    game.style.gridTemplateColumns = `repeat(${col}, auto)`;
    if (game.children.length !== 0) {
      for (let i = game.children.length - 1; i >= 0; i--) {
        game.children[i].remove();
      }
    }
    for (let i = 1; i <= row * col; i++) {
      stack.push(counter);
      card = new Card(counter, array[counter],checkMatch,checkPause,checkWin);
      counter++;
    }
  }

  function checkPause() {
    return this.pause ;
  }

  function removeValue(array,value) {
    let index = array.indexOf(Number(value));
    if (index > -1) {
      array.splice(index, 1);
    }
  }

  function checkMatch(card, value, addMatched, removePicked) {
    let isMatched = false;
    if (open.length === 0) open.push({'card': card, 'value': value});
    else if (open[0].card !== card) {
      open.push({'card': card, 'value': value});
      isMatched = (open[0].value === open[1].value);
      if (isMatched) {
        addMatched(open[0].card.firstChild);
        addMatched(open[1].card.firstChild);
        removeValue(stack, open[0].card.id);
        removeValue(stack, open[1].card.id);
        open = [];
      } else {
        this.pause = true;
        setTimeout(function () {
          removePicked(open[0].card.firstChild);
          removePicked(open[1].card.firstChild);
          open = [];
          this.pause = false;
        }, 600);
      }
    }
    return stack;
  }

  function checkWin(array,picked){
    if (array.length === 0) {
      modal = new Modal().showPanelModal();
    }
    if(array.length === 1){
      let card = document.getElementById(`${stack[0]}`).firstChild;
      picked(card, Math.ceil((row * col) / 2));
      setTimeout(function () {
        modal = new Modal().showPanelModal();
      }, 600);
    }
  }
};

